var fs = require('fs');
import path from 'path';
import { app, remote } from 'electron';
import os from 'os'

let appRootDir = require('app-root-dir').get().replace('app.asar', '').replace(/(\s+)/g, '\\$1');
export const rootDir = require('app-root-dir').get()
function getPlatform(){
    switch (os.platform()) {
        case 'aix':
        case 'freebsd':
        case 'linux':
        case 'openbsd':
        case 'android':
          return 'linux';
        case 'darwin':
        case 'sunos':
          return 'mac';
        case 'win32':
          return 'win';
      }
}

export const platform = getPlatform()

const IS_PROD = process.env.NODE_ENV === 'production';
const root = process.cwd();
const APP = process.type === 'renderer' ? remote.app : app

const binariesPath =
  IS_PROD || APP.isPackaged
    ? path.join(process.resourcesPath, 'bin', platform)
    : path.join(root, 'resources', 'bin', platform);

const epicBinaries = platform==='win'?'epic-wallet.exe':'epic-wallet'
export let epicPath = path.join(binariesPath, epicBinaries)
if(platform=='win'){
  epicPath = '"' + path.resolve(epicPath) + '"'
}
export const chainType = 'main'
export const epicNode = "http://ip:3413"
export const epicNode2 = "http://ip:3413"
export const epicDIR = path.join(APP.getPath('home'), '.epic')
export const seedPath = path.join(APP.getPath('home'), '.epic', chainType, 'wallet_data/wallet.seed')
export const walletTOMLPath = path.join(APP.getPath('home'), '.epic', chainType, 'epic-wallet.toml')
export const walletPath = path.join(APP.getPath('home'), '.epic', chainType)
export const apiSecretPath = path.join(APP.getPath('home'), '.epic', chainType, '.api_secret')
export const ewalletPath = path.join(APP.getPath('home'), '.epic')
export const logDir = path.join(ewalletPath, 'log')
export const tempTxDir = path.join(ewalletPath, 'temp_tx')

export const configPath = path.join(ewalletPath, 'config.json')



export function getConfig(){
  try{
    return JSON.parse(fs.readFileSync(configPath))
  }catch (e) {
    return {}
  }
}

export function setConfig(options){
  return fs.writeFileSync(configPath, JSON.stringify(options))
}

export function updateConfig(options){
  let options_ = getConfig()
  for(var key in options){
    options_[key] = options[key]
  }
  setConfig(options_)
}

//export const logLevel = getConfig()['debug']?'debug':'info'
export const logLevel = 'debug'

//export const hedwigServer = 'https://v1.hedwig.im'
//export const hedwigClient =
//  IS_PROD || APP.isPackaged
  //  ? path.resolve(path.join(process.resourcesPath, 'bin', 'hedwig', 'client.js'))
  //  : path.resolve(path.join(root, 'hedwig', 'client.js'))

export const hedwigApp = 'Epic Cash Wallet'

//export const epicRsWallet =
  //IS_PROD || APP.isPackaged
    //? path.resolve(path.join(process.resourcesPath, 'bin', 'epicRs', 'wallet.js'))
    //: path.resolve(path.join(root, 'epicRs', 'wallet.js'))

//export const nodeExecutable =
  //  IS_PROD || APP.isPackaged
    //  ? path.resolve(path.join(process.resourcesPath, 'bin', 'epicRs', 'node.exe'))
    //  : path.resolve(path.join(root, 'epicRs', 'node.exe'))


function getLocale(){
  let locale = getConfig()['locale']
  if(locale)return locale
  locale = APP.getLocale().toLowerCase()
  if(locale.startsWith('zh'))return 'zh'
  if(locale.startsWith('ru'))return 'ru'
  return 'en'
}
export function setLocale(locale){
  updateConfig({'locale':locale})
}
export const locale = getLocale()
export const langs = {'en':'English'}
