var fs = require('fs');
import { ewalletPath, logDir, getConfig, setConfig, updateConfig, tempTxDir} from './config';

export function checkFirstTime(){
    console.log(ewalletPath)
    const isFirstTime = fs.existsSync(ewalletPath)?false:true
    if(isFirstTime){
        fs.mkdirSync(ewalletPath)
        fs.mkdirSync(logDir)
        fs.mkdirSync(tempTxDir)
        setConfig({'firstTime':true})
    }
    else{
        updateConfig({'firstTime':false})
    }
}

export function isFirstTime(){
    return getConfig()['firstTime']
}

